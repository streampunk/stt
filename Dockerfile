
# Build VOSK using this very very old centos 6-based image.
# On Debian 11, the CLAPACK build fails otherwise.
FROM quay.io/pypa/manylinux2010_x86_64 AS build

RUN yum -y update && yum -y install \
    devtoolset-8-libatomic-devel \
    automake \
    autoconf \
    libtool \
    cmake \
    && yum clean all

RUN cd /opt \
    && git clone -b vosk --single-branch https://github.com/alphacep/kaldi \
    && cd /opt/kaldi/tools \
    && git clone -b v0.3.20 --single-branch https://github.com/xianyi/OpenBLAS \
    && git clone -b v3.2.1  --single-branch https://github.com/alphacep/clapack \
    && make -C OpenBLAS ONLY_CBLAS=1 DYNAMIC_ARCH=1 TARGET=NEHALEM USE_LOCKING=1 USE_THREAD=0 all \
    && make -C OpenBLAS PREFIX=$(pwd)/OpenBLAS/install install \
    && mkdir -p clapack/BUILD && cd clapack/BUILD && cmake .. && make -j 10 && find . -name "*.a" | xargs cp -t ../../OpenBLAS/install/lib \
    && cd /opt/kaldi/tools \
    && git clone --single-branch https://github.com/alphacep/openfst openfst \
    && cd openfst \
    && autoreconf -i \
    && CFLAGS="-g -O3" ./configure --prefix=/opt/kaldi/tools/openfst --enable-static --enable-shared --enable-far --enable-ngram-fsts --enable-lookahead-fsts --with-pic --disable-bin \
    && make -j 10 && make install \
    && cd /opt/kaldi/src \
    && ./configure --mathlib=OPENBLAS_CLAPACK --shared --use-cuda=no \
    && sed -i 's:-msse -msse2:-msse -msse2:g' kaldi.mk \
    && sed -i 's: -O1 : -O3 :g' kaldi.mk \
    && make -j $(nproc) online2 lm rnnlm \
    && find /opt/kaldi -name "*.o" -exec rm {} \;

RUN cd /opt \
    && git clone https://github.com/alphacep/vosk-api \
    && cd vosk-api/src \
    && KALDI_ROOT=/opt/kaldi OPENFST_ROOT=/opt/kaldi/tools/openfst OPENBLAS_ROOT=/opt/kaldi/tools/OpenBLAS/install make -j $(nproc)

FROM golang:1.17.3 AS build-go

COPY --from=build /opt/vosk-api/src/libvosk.so /usr/local/lib/
COPY --from=build /opt/vosk-api/src/vosk_api.h /usr/local/include/

ADD . /src
WORKDIR /src
RUN go build ./cmd/stt \
    && go build ./cmd/update-models

FROM debian:stable-slim

COPY --from=build /opt/vosk-api/src/libvosk.so /usr/local/lib/
COPY --from=build /opt/vosk-api/src/vosk_api.h /usr/local/include/
COPY --from=build-go /src/stt /usr/bin/
COPY --from=build-go /src/update-models /usr/bin/

RUN apt-get -q update \
    && env DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends ffmpeg \
    && apt clean \
    && rm -fr /var/lib/apt/lists/* \
    && /sbin/ldconfig
