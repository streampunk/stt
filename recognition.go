package stt

import (
	"bufio"
	"io"
	"log"
	"os/exec"
	"strconv"

	vosk "github.com/alphacep/vosk-api/go"
)

const (
	sampleRate = 16000
	bufSize    = 4096
)

type sttStream struct {
	decoder *exec.Cmd
	rec     *vosk.VoskRecognizer
	textCh  chan string
	errCh   chan error
}

func newSTTStream(uri, lang string, mmgr *modelManager) (*sttStream, error) {
	model, err := mmgr.getModel(lang)
	if err != nil {
		return nil, err
	}
	recognizer, err := vosk.NewRecognizer(model, sampleRate)
	if err != nil {
		return nil, err
	}

	cmd := exec.Command(
		"ffmpeg", "-loglevel", "quiet",
		"-i", uri,
		"-ar", strconv.Itoa(sampleRate), "-ac", "1",
		"-f", "s16le", "-")

	stdout, err := cmd.StdoutPipe()
	if err != nil {
		return nil, err
	}

	log.Printf("stt: starting decoder for %s (%s)", uri, lang)
	if err := cmd.Start(); err != nil {
		return nil, err
	}

	s := &sttStream{
		decoder: cmd,
		rec:     recognizer,
		textCh:  make(chan string),
		errCh:   make(chan error, 1),
	}
	go func() {
		s.errCh <- s.run(stdout)
		close(s.errCh)
	}()

	return s, nil
}

func (s *sttStream) Close() error {
	s.decoder.Process.Kill() // nolint: errcheck
	return <-s.errCh
}

func (s *sttStream) run(input io.ReadCloser) error {
	defer close(s.textCh)

	reader := bufio.NewReader(input)
	buf := make([]byte, bufSize)

	for {
		_, err := io.ReadFull(reader, buf)
		if err != nil {
			if err == io.EOF {
				log.Printf("stt: stream closed")
				return nil
			}
			log.Printf("stt: closing stream: %v", err)
			return err
		}

		if s.rec.AcceptWaveform(buf) != 0 {
			res := string(s.rec.Result())
			log.Printf("stt: result: %s", res)
			s.textCh <- res
		}
	}
}
