Stream To Text
===

This tool uses speech recognition to transcribe audio streams in real
time. It uses [VOSK](https://alphacephei.com/vosk/) for recognition,
supporting many different languages (though you have to specify the
language upfront).

The tool operates as a HTTP server, with a single endpoint accepting
*url* and *lang* query parameters, e.g.:

```
https://localhost:3242/?url=https://s.streampunk.cc/ondarossa.ogg&lang=it
```

the transcribed response will be returned as a sequence of HTTP/1.1
chunked response parts.
