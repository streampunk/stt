package stt

import (
	"encoding/json"
	"errors"
	"io/ioutil"
	"path/filepath"
	"sync"

	vosk "github.com/alphacep/vosk-api/go"
)

type modelManager struct {
	dir     string
	langMap map[string]string
	cacheMx sync.Mutex
	cache   map[string]*vosk.VoskModel
}

func loadModelManager(langMapPath string) (*modelManager, error) {
	mapJSON, err := ioutil.ReadFile(langMapPath)
	if err != nil {
		return nil, err
	}
	var langMap map[string]string
	if err := json.Unmarshal(mapJSON, &langMap); err != nil {
		return nil, err
	}
	return newModelManager(filepath.Dir(langMapPath), langMap), nil
}

func newModelManager(dir string, langMap map[string]string) *modelManager {
	return &modelManager{
		dir:     dir,
		langMap: langMap,
		cache:   make(map[string]*vosk.VoskModel),
	}
}

func (m *modelManager) getModel(lang string) (*vosk.VoskModel, error) {
	name, ok := m.langMap[lang]
	if !ok {
		return nil, errors.New("unsupported language")
	}

	m.cacheMx.Lock()
	defer m.cacheMx.Unlock()

	model, ok := m.cache[lang]
	if !ok {
		var err error
		model, err = vosk.NewModel(filepath.Join(m.dir, name))
		if err != nil {
			return nil, err
		}
		m.cache[lang] = model
	}
	return model, nil
}
