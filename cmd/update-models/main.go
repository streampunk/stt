package main

import (
	"archive/zip"
	"encoding/json"
	"errors"
	"flag"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"strings"
)

var (
	modelListURL = "https://alphacephei.com/vosk/models/model-list.json"
	destdir      = flag.String("destdir", ".", "destination directory")
	typeFilter   = flag.String("type", "small", "download only models of this type")
	langFilter   = flag.String("lang", "", "only download this language")
)

type model struct {
	Lang     string `json:"lang"`
	Name     string `json:"name"`
	URL      string `json:"url"`
	Type     string `json:"type"`
	Obsolete string `json:"obsolete"`
}

func listModels() ([]*model, error) {
	var models []*model

	resp, err := http.Get(modelListURL)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	if err := json.NewDecoder(resp.Body).Decode(&models); err != nil {
		return nil, err
	}
	return models, nil
}

func downloadTempFile(url string) (*os.File, int64, error) {
	resp, err := http.Get(url)
	if err != nil {
		return nil, 0, err
	}
	defer resp.Body.Close()
	if resp.StatusCode != 200 {
		return nil, 0, fmt.Errorf("HTTP error %d", resp.StatusCode)
	}

	tmpf, err := ioutil.TempFile("", "")
	if err != nil {
		return nil, 0, err
	}
	os.Remove(tmpf.Name())

	sz, err := io.Copy(tmpf, resp.Body)
	if err != nil {
		return nil, 0, err
	}

	_, err = tmpf.Seek(0, 0)
	return tmpf, sz, err
}

func downloadModel(m *model) error {
	dst := filepath.Join(*destdir, m.Name)
	if _, err := os.Stat(dst); err == nil {
		return nil
	}

	tmpf, sz, err := downloadTempFile(m.URL)
	if err != nil {
		return err
	}
	defer tmpf.Close()

	zf, err := zip.NewReader(tmpf, sz)
	if err != nil {
		return err
	}

	for _, f := range zf.File {
		filePath := filepath.Join(*destdir, f.Name)
		if !strings.HasPrefix(filePath, filepath.Clean(*destdir)+string(os.PathSeparator)) {
			return errors.New("invalid file path in archive")
		}
		if f.FileInfo().IsDir() {
			os.MkdirAll(filePath, os.ModePerm)
			continue
		}

		if err := os.MkdirAll(filepath.Dir(filePath), os.ModePerm); err != nil {
			return err
		}

		dstFile, err := os.OpenFile(filePath, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, f.Mode())
		if err != nil {
			return err
		}

		fileInArchive, err := f.Open()
		if err != nil {
			return err
		}

		if _, err := io.Copy(dstFile, fileInArchive); err != nil {
			return err
		}

		dstFile.Close()
		fileInArchive.Close()
	}

	return nil
}

func main() {
	log.SetFlags(0)
	flag.Parse()

	models, err := listModels()
	if err != nil {
		log.Fatal(err)
	}

	langMap := make(map[string]string)
	for _, m := range models {
		if m.Obsolete == "true" {
			continue
		}
		if *typeFilter != "" && *typeFilter != m.Type {
			continue
		}
		if *langFilter != "" && *langFilter != m.Lang {
			continue
		}
		log.Printf("downloading %s (%s, %s)", m.Name, m.Lang, m.Type)
		if err := downloadModel(m); err != nil {
			log.Fatal(err)
		}

		langMap[m.Lang] = m.Name
	}

	json.NewEncoder(os.Stdout).Encode(langMap)
}
