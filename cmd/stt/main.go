package main

import (
	"flag"
	"log"
	"net/http"

	"git.autistici.org/streampunk/stt"
)

var (
	addr       = flag.String("addr", ":3242", "address to listen on")
	modelsJSON = flag.String("models", "models.json", "JSON model metadata file")
	streamRx   = flag.String("stream-rx", `^https://s\.streampunk\.cc/[^/]+$`, "regexp for remote stream URLs")
)

func main() {
	log.SetFlags(0)
	flag.Parse()

	h, err := stt.NewHandler(*modelsJSON, *streamRx)
	if err != nil {
		log.Fatal(err)
	}

	http.HandleFunc("/", func(w http.ResponseWriter, req *http.Request) {
		if req.URL.Path != "/" {
			http.NotFound(w, req)
			return
		}
		h.ServeHTTP(w, req)
	})

	if err := http.ListenAndServe(*addr, nil); err != nil {
		log.Fatal(err)
	}
}
