package stt

import (
	"container/list"
	"fmt"
	"log"
	"sync"
)

type subscription struct {
	c <-chan string

	mgr *streamManager
	el  *list.Element
}

func (s *subscription) Next() string {
	return <-s.c
}

func (s *subscription) Close() {
	s.mgr.removeSubscription(s.el)
}

const (
	streamStateStopped = iota
	streamStateRunning
)

type streamManager struct {
	uri  string
	lang string
	mmgr *modelManager

	mx          sync.Mutex
	subCount    int
	subscribers *list.List
	streamState int
	stream      *sttStream
}

func newStreamManager(uri, lang string, mmgr *modelManager) *streamManager {
	return &streamManager{
		uri:         uri,
		lang:        lang,
		mmgr:        mmgr,
		streamState: streamStateStopped,
		subscribers: list.New(),
	}
}

func (m *streamManager) newSubscription() (*subscription, error) {
	doStart := false

	m.mx.Lock()

	c := make(chan string, 100)
	el := m.subscribers.PushBack(c)
	m.subCount++
	if m.subCount > 0 && m.streamState == streamStateStopped {
		// Start the stream (when not holding the lock, though).
		log.Printf("starting stream for %s (%s)", m.uri, m.lang)
		m.streamState = streamStateRunning
		doStart = true
	}

	m.mx.Unlock()

	if doStart {
		go func() {
			if err := m.runStreamUntilStopped(m.uri, m.lang, m.mmgr); err != nil {
				log.Printf("stream error: %v", err)
			}
		}()
	}

	return &subscription{
		c:   c,
		mgr: m,
		el:  el,
	}, nil
}

func (m *streamManager) removeSubscription(el *list.Element) {
	m.mx.Lock()
	defer m.mx.Unlock()

	m.subscribers.Remove(el)
	m.subCount--
	if m.subCount == 0 && m.stream != nil {
		// Stop the stream.
		log.Printf("stopping stream for %s (%s)", m.uri, m.lang)
		m.streamState = streamStateStopped
		m.stream.Close()
	}
}

func (m *streamManager) runStreamUntilStopped(uri, lang string, mmgr *modelManager) error {
	for {
		err := m.runStream(uri, lang, mmgr)
		if err != nil {
			return err
		}

		m.mx.Lock()
		state := m.streamState
		m.mx.Unlock()
		if state != streamStateRunning {
			return nil
		}
	}
}

func (m *streamManager) runStream(uri, lang string, mmgr *modelManager) error {
	stream, err := newSTTStream(uri, lang, mmgr)
	if err != nil {
		return err
	}
	defer stream.Close()

	m.mx.Lock()
	m.stream = stream
	m.mx.Unlock()
	defer func() {
		m.mx.Lock()
		m.stream = nil
		m.mx.Unlock()
	}()

	for text := range stream.textCh {
		m.mx.Lock()
		for el := m.subscribers.Front(); el != nil; el = el.Next() {
			c := el.Value.(chan string)
			select {
			case c <- text:
			default:
			}
		}
		m.mx.Unlock()
	}

	return nil
}

type dispatcher struct {
	mmgr *modelManager

	mx      sync.Mutex
	streams map[string]*streamManager
}

func newDispatcher(mmgr *modelManager) *dispatcher {
	return &dispatcher{
		mmgr:    mmgr,
		streams: make(map[string]*streamManager),
	}
}

func (d *dispatcher) getStream(uri, lang string) (*streamManager, error) {
	d.mx.Lock()
	defer d.mx.Unlock()

	key := fmt.Sprintf("%s:%s", lang, uri)
	s, ok := d.streams[key]
	if !ok {
		s = newStreamManager(uri, lang, d.mmgr)
		d.streams[key] = s
	}
	return s, nil
}
