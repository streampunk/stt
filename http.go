package stt

import (
	"io"
	"log"
	"net/http"
	"regexp"
)

const defaultLanguage = "it"

type Handler struct {
	*dispatcher

	urlRx *regexp.Regexp
}

func NewHandler(langMapPath, remoteURLRx string) (*Handler, error) {
	mmgr, err := loadModelManager(langMapPath)
	if err != nil {
		return nil, err
	}

	urlRx, err := regexp.Compile(remoteURLRx)
	if err != nil {
		return nil, err
	}

	return &Handler{
		dispatcher: newDispatcher(mmgr),
		urlRx:      urlRx,
	}, nil
}

func (h *Handler) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	streamURL := req.FormValue("url")
	if !h.urlRx.MatchString(streamURL) {
		http.Error(w, "bad request", http.StatusBadRequest)
		return
	}

	lang := req.FormValue("lang")
	if lang == "" {
		lang = defaultLanguage
	}

	s, err := h.dispatcher.getStream(streamURL, lang)
	if err != nil {
		log.Printf("getStream(%s, %s): %v", streamURL, lang, err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	sub, err := s.newSubscription()
	if err != nil {
		log.Printf("newSubscription(%s, %s): %v", streamURL, lang, err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	defer sub.Close()

	w.Header().Set("Content-Type", "text/plain")
	w.WriteHeader(http.StatusOK)

	flusher := w.(http.Flusher)
	for {
		jsonResult := sub.Next()
		if jsonResult == "" {
			break
		}
		if _, err := io.WriteString(w, jsonResult+"\n"); err != nil {
			break
		}
		flusher.Flush()
	}
}
